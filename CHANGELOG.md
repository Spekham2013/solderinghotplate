## v0.2.0
- 2022-06-08 Added functionality to enter and exit programs
- 2022-06-08 Added implemented RTIC in main.rs using TIMER0 as tick source
- 2022-06-08 Changed dependicies of soldering_hotplate (added rp2040-monotonic)
- 2022-06-08 Fixed common library acces of modules
- 2022-06-08 Added get_temperature function to MLX90614 and incorporated correct error handling
- 2022-06-08 Added settings.json to fix stuck build with rust_analyzer

## v0.1.0
- 2022-06-07 Added Release Job to gitlab ci
- 2022-06-06 Added PC_Logger.py for logging uart data and timestamping it
- 2022-06-06 Added MAX6675_Sensor.rs for interfacing with the SPI thermocouple amplifier
- 2022-06-05 Added bounded output to pid.rs
- 2022-06-05 Changed pid unit testing to do something usefull 
- 2022-06-05 Added .vscode for debugging and building in vscode with j-link
- 2022-06-05 Added MLX90614_Sensor.rs driver to the MLX sensor
- 2022-05-26 Added pid.m for computation of PID loop in matlab
- 2022-05-26 Added pid.rs rust implementation of a PID loop