# SolderingHotplate

## Building the project
Setup:
```
rustup default nightly
rustup update
rustup target install thumbv6m-none-eabi
cargo install flip-link
cargo install probe-run
cargo install elf2uf2-rs
```
Run unit tests
```
cargo test -p common
```
Build binaries
```
cargo build -p soldering_hotplate
```
Run on raspberry pi pico using usb
```
cd soldering_hotplate
cargo run
```

## IDE
VSCode is used as development environment, using the rust-analyzer extension. The program can be debugged using a segger j-link and the cortex debug extension for vscode, the files needed for this are in the .vscode folder.

## Roadmap

### v0.1.0
- [X] Basic logging over UART0
- [X] MAX6675 spi interfacing
- [X] MLX90614 I2C infrared sensor interfacing
- [X] Basic PID loop with unit testing
- [X] Status leds and buttons
- [X] PC side logging of uart temperature data

### v0.2.0
- [X] Temperature profile stored in flash/code (sort of)
- [X] Executing temperature profile
- [X] RTIC implementation

### v0.3.0
- [ ] Tune PID loop
- [ ] Loading temperature profile via usb
- [ ] Interface on PC to track temperature profile
- [ ] Designing temperature profile on PC with sliders in a graph
- [ ] View logging on PC

### v0.4.0
- [ ] Usage of dual cores on for PC interface other for controller
  
## Sources
- PICO resources
    - https://nnarain.github.io/2021/08/02/Setting-up-per-package-targets-in-Rust.html
    - https://reltech.substack.com/p/getting-started-with-rust-on-a-raspberry-a88?s=r
    - https://wiki.segger.com/Raspberry_Pi_Pico
- Documentation
    - https://crates.io/crates/rp-pico
- RTIC
    - https://github.com/joaocarvalhoopen/Raspberry_Pi_Pico_in_Rust__Proj_Template_with_RTIC_USB-Serial_UF2/blob/main/src/main.rs
    - https://github.com/sonnny/rtic_template/blob/main/src/setup.rs
- PID
    - https://github.com/korfuri/PIDController
    - https://github.com/korfuri/PIDController/blob/3bb5882acd23a074cb012368e77f99e74ab5dbb9/pidcontroller_test.py#L53