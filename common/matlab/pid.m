s = tf('s');
P = 1/(s^2 + 10*s + 20);

Kp = 350;
Ki = 500;
Kd = 5;
C = pid(Kp,Ki,Kd);
T = feedback(C*P,1);

t = 0:0.01:2;
values = step(T,t);
writematrix(values,'pid_values.csv') 