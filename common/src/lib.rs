#![cfg_attr(not(test), no_std)]

#[cfg(test)] 
use assert_float_eq::*;

pub mod pid;
#[allow(unused_imports)]
pub use pid::{Pid, PidLoop};

// =============================================================================
// PID Unit tests

#[test]
// No correction should happen if steady state is reached
fn test_pid_steady_state_error() {
    const DELTA_T: f64 = 0.01;
    let mut pidval: pid::PidLoop = pid::Pid::new(1.0, 1.0, 1.0, DELTA_T);
    assert_float_absolute_eq!(pidval.run(0.0, 0.0), 0.0, 1e-6);
}

#[test]
// Test if the output of the system is bounded
fn test_pid_bounded_output() {
    const DELTA_T: f64 = 0.01;
    let mut pidval: pid::PidLoop = pid::Pid::new(1.0, 1.0, 1.0, DELTA_T);

    const MAX_OUTPUT: f64 = 100.0;
    const MIN_OUTPUT: f64 = 0.0;
    pidval.set_min_max(MIN_OUTPUT, MAX_OUTPUT);

    assert_float_absolute_eq!(pidval.run(0.0, 100.0),  MAX_OUTPUT, 1e-6);
    assert_float_absolute_eq!(pidval.run(0.0, -100.0), MIN_OUTPUT, 1e-6);
}

#[test]
// Test the final value of an instantly responding system
fn test_pid_instant_response() {
    const DELTA_T: f64 = 0.01;
    let mut pidval: PidLoop = Pid::new(0.5, 0.01, 0.1, DELTA_T);
    pidval.set_min_max(0.0, 10.0);

    let mut state: f64 = 0.0;
    let mut correction: f64 = 0.0;

    const SET_POINT: f64 = 100.0;
    for _n in 0..100 {  
        state += correction;
        correction = pidval.run(state, SET_POINT);
    }

    assert_float_absolute_eq!(state, SET_POINT, 1.0);
}