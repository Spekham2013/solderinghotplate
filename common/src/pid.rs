pub struct PidLoop {
    kp: f64,
    ki: f64,
    kd: f64,
    delta_t: f64,

    // Internal things
    last_error: f64,
    integral: f64,
    derivative: f64,
    minimum_output: Option<f64>,
    maximum_output: Option<f64>,
}

pub trait Pid {
    fn new(kp: f64, ki: f64, kd: f64, delta_t: f64) -> Self;
    fn run(&mut self, input: f64, set_point: f64) -> f64;
    fn set_min_max(&mut self, minimum: f64, maximum: f64);
}

impl Pid for PidLoop {
    fn new(kp: f64, ki: f64, kd: f64, delta_t: f64) -> Self {
        PidLoop { 
            kp, 
            ki, 
            kd, 
            delta_t,
            last_error: 0.0, 
            integral:   0.0, 
            derivative: 0.0,
            minimum_output: None,
            maximum_output: None,
        }
    }

    fn run(&mut self, input: f64, set_point: f64) -> f64 {
        let error: f64 = set_point - input;
        
        self.integral += error;
        self.derivative = error - self.last_error;

        let mut output: f64 = (self.kp * error) + (self.ki * self.integral * self.delta_t) + ((self.kd * self.derivative) / self.delta_t);

        // Cap the output if min/max is defined
        if let Some(minimum) = self.minimum_output {
            if let Some(maxmimum) = self.maximum_output {
                if output > maxmimum {
                    output = maxmimum;
                } else if output < minimum {
                    output = minimum;
                }
            }
        }

        self.last_error = error;
        output
    }

    fn set_min_max(&mut self, minimum: f64, maximum: f64) {
        self.minimum_output = Some(minimum);
        self.maximum_output = Some(maximum);
    }
}
