import serial
from datetime import datetime

# Serial port variables
port = "/dev/ttyACM0"
baud = 115200 

# Create the filename
timestampFile = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
fileName="{}.csv".format(timestampFile)

# Open the port
ser = serial.Serial(port, baud)
file = open(fileName, "a")
print("Connected to port: {}".format(port))
print("Created file: {}".format(fileName))
file.close()

file = open(fileName, "a")
counter = 1
while 1:
    # Close file every 100 datapoints
    if counter == 100:
        counter = 1
        file.flush()

    # Log data with second timestamp
    getData=ser.readline().strip().decode('ascii')
    timestamp = datetime.now().strftime("%Y-%m-%d_%H:%M:%S")

    string = "{}, {}".format(timestamp, getData)

    file.write(string + "\n")
    print(string)
    counter += 1

# Shall never be reached but using control - c will still close file correctly
print("Data collection complete!")
file.close()