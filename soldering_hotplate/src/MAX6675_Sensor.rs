// =============================================================================
// Fix stupid clippy
#![allow(unused_must_use)]
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(dead_code)]

// =============================================================================
// Imports
use embedded_hal::blocking::spi::Transfer;
use embedded_hal::digital::v2::OutputPin;

// =============================================================================
// General MAX6675 Struct
pub struct MAX6675<SPI, CS> {
    spi: SPI,
    cs: CS,
}

// Errors of MLX90614
#[derive(Debug)]
pub enum Error<E> {
    SPi(E),
    OpenThermocouple,
}

// =============================================================================
// MAX6675 Implementation
impl<SPI, CS, E> MAX6675<SPI, CS> where SPI: Transfer<u8, Error = E>, CS: OutputPin {
    // =============================================================================
    // Public functions
    pub fn new(spi: SPI, cs: CS) -> Self {
        MAX6675 {
            spi,
            cs
        }
    }

    pub fn get_temperature(&mut self) -> Result<f32, Error<E>> {
        // Enable IC
        self.cs.set_low();

        let mut buffer: [u8; 2] = [0; 2];
        self.spi.transfer(&mut buffer).map_err(Error::SPi)?;

        // Disable IC
        self.cs.set_high();

        // Process data
        if buffer[1] & (1 << 2) != 0 {
            return Err(Error::OpenThermocouple);
        }

        let register_value: u16 = (buffer[0] as u16) << 5 | ((buffer[1] as u16) >> 3);
        let temperature: f32 = register_value as f32 * 0.25;

        Ok(temperature)
    }
}