// =============================================================================
// Fix stupid clippy
#![allow(non_camel_case_types)]
#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(dead_code)]

// =============================================================================
// Imports
extern crate embedded_hal as hal;
use hal::blocking::i2c::{Write, WriteRead};
// use hal::blocking::delay::{DelayUs, DelayMs};
use byteorder::{ByteOrder, LittleEndian};

// =============================================================================
// Register definitions

// Register types
const RAM_ACCESS:           u8 = 0x00;
const EEPROM_ACCESS:        u8 = 0x20;

// EEPROM registers
const MLX_REG_TO_MAX:       u8 = EEPROM_ACCESS;         // Write access (0x00)
const MLX_REG_TO_MIN:       u8 = EEPROM_ACCESS | 0x01;  // Write access  
const MLX_REG_PWM_CTRL:     u8 = EEPROM_ACCESS | 0x02;  // Write access
const MLX_REG_TA_RANGE:     u8 = EEPROM_ACCESS | 0x03;  // Write access
const MLX_REG_EMISSIVITY:   u8 = EEPROM_ACCESS | 0x04;  // Write access
const MLX_REG_CONF_1:       u8 = EEPROM_ACCESS | 0x05;  // Write access
const MLX_REG_SMBus:        u8 = EEPROM_ACCESS | 0x0E;  // Write access
const MLX_REG_ID_0:         u8 = EEPROM_ACCESS | 0x1C;  // No write access
const MLX_REG_ID_1:         u8 = EEPROM_ACCESS | 0x1D;  // No write access
const MLX_REG_ID_2:         u8 = EEPROM_ACCESS | 0x1E;  // No write access
const MLX_REG_ID_3:         u8 = EEPROM_ACCESS | 0x1F;  // No write access

// RAM Registers
const MLX_REG_IR_CHANN_0:   u8 = RAM_ACCESS | 0x04;
const MLX_REG_IR_CHANN_1:   u8 = RAM_ACCESS | 0x05;
const MLX_REG_TA:           u8 = RAM_ACCESS | 0x06;
const MLX_REG_T_OBJ_0:      u8 = RAM_ACCESS | 0x07;
const MLX_REG_T_OBJ_1:      u8 = RAM_ACCESS | 0x08;

// =============================================================================
// General MLX90614 Struct
pub struct MLX90614<I2C> {
    i2c: I2C,
    i2c_address: u8,
}

// Errors of MLX90614
#[derive(Debug)]
pub enum Error<E> {
    I2c(E),
    InvalidCRC,
    InvalidData,
}

// =============================================================================
// MLX90614 Implementation
impl<I2C, E> MLX90614<I2C> where I2C: WriteRead<Error = E> + Write<Error = E>, {
    // =============================================================================
    // Public functions
    pub fn new(i2c: I2C, address: u8) -> Self {
        MLX90614 {
            i2c,
            i2c_address: address,
        }
    }

    pub fn get_temperature(&mut self) -> Result<(f32, f32), Error<E>> {
        let temp_ir_0 = self.get_object_temp(0);
        let temp_ir_0 = match temp_ir_0 {
            Ok(temp) => temp,
            // TODO: Fix this to return the error instead of a static one
            Err(_e) => return Err(Error::InvalidData),
        };

        let temp_amb = self.get_ambiant_temp();
        let temp_amb = match temp_amb {
            Ok(temp) => temp,
            // TODO: Fix this to return the error instead of a static one
            Err(_e) => return Err(Error::InvalidData),
        };

        Ok((temp_ir_0, temp_amb))
    }

    // =====================<========================================================
    // Private functions
    fn get_ambiant_temp(&mut self) -> Result<f32, E> {
        // Read in two bytes of which the last is the CRC
        let mut buffer: [u8; 2] = [0; 2];
        self.ram_read(MLX_REG_TA, &mut buffer)?;

        let register_value = LittleEndian::read_u16(&buffer[0..2]);
        let temperature: f32 = (register_value as f32 * 0.02) - 273.15;

        Ok(temperature)
    }

    fn get_object_temp(&mut self, object: u8) -> Result<f32, Error<E>> {
        // Read in three bytes of which the last is the CRC
        let mut buffer: [u8; 2] = [0; 2];

        let mut register = MLX_REG_T_OBJ_0;
        if object == 1 {
            register = MLX_REG_T_OBJ_1;
        }

        self.ram_read(register, &mut buffer).map_err(Error::I2c)?;

        if buffer[1] & (1 << 7) != 0 {
            return Err(Error::InvalidData);
        }

        let register_value = LittleEndian::read_u16(&buffer[0..2]);
        let temperature: f32 = (register_value as f32 * 0.02) - 273.15;

        Ok(temperature)
    }

    // fn eeprom_read(&mut self, register: u8, data: &mut [u8; 2]) -> Result<(), E> {
    //     self.ram_read(register, data)
    // }

    // fn eeprom_write(&mut self, delay: &mut (impl DelayUs<u16> + DelayMs<u16>), register: u8, data: &[u8]) -> Result<(), E> {
    //     // Write 0x0000 into the cell of interest (effectively erasing the cell)
    //     let data_erase: u8 = concat(&a, data);
    //     self.i2c.write(self.i2c_address, &[register, data])

    //     // Wait at least 5ms (10ms to be on the safe side)
    //     delay.DelayMs(10);

    //     // Write the new value
    //     let data_write
    //     self.i2c.write(self.i2c_address, &[register, data])

    //     // Wait at least 5ms (10ms to be on the safe side)
    //     delay.DelayMs(10);

    //     // Read back and compare if the write was successful
    // }
    
    // TODO: Implement CRC checking
    fn ram_read(&mut self, register: u8, data: &mut [u8; 2]) -> Result<(), E> {
        let mut data_read: [u8; 3] = [0; 3];
        self.i2c.write_read(self.i2c_address, &[register], &mut data_read)?;

        // CRC checking

        // Return the data
        data[0] = data_read[0];
        data[1] = data_read[1];

        Ok(())
    }

    // fn ram_write(&mut self, register: u8, data: &[u8; 2]) -> Result<(), E> {
    //     let data_write: [u8; 3] = [register, data[0], data[1]];
    //     self.i2c.write(self.i2c_address, &data_write)
    // }
}
