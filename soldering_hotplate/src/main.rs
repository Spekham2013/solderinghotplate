#![no_std]
#![no_main]

// Fix stupid clippy
#![allow(clippy::type_complexity)]

// System libraries
use core::fmt::Write;
// use cortex_m_rt::entry;

use embedded_hal::{
    digital::v2::*, 
    spi::MODE_0
};
use embedded_time::{
    // fixed_point::FixedPoint, 
    rate::Extensions,
};

// use defmt::*;
use defmt_rtt as _;

use rp_pico::hal::{
    clocks::{init_clocks_and_plls, Clock},
    i2c::I2C,
    gpio,
    pac,
    sio::Sio,
    watchdog::Watchdog,
};
use panic_probe as _;

// RTIC
use rtic::app;
use systick_monotonic::*;
use rp2040_monotonic::Rp2040Monotonic;

// User libraries
mod MLX90614_Sensor;
use MLX90614_Sensor::MLX90614;

mod MAX6675_Sensor;
use MAX6675_Sensor::MAX6675;

#[allow(unused_imports)]
use common::pid::{PidLoop, Pid};

// The program
#[app(device = rp_pico::hal::pac, dispatchers = [XIP_IRQ])]
mod app {
    use common::Pid;
    use rp_pico::hal::uart::UartPeripheral;

    use super::*;

    #[shared]
    struct Shared {
        system_state: u8,
        // 0 is the system is off
        // 1 is (Program0) the profile is 60 -> 250
        // 2 is (Program1) the profile is 250
        temp_tc: f32,
        temp_ir: f32,
        temp_amb: f32,
        temp_target: f32,
        duty_cycle: u8,
    }

    #[local]
    struct Local {
        pid_loop:   PidLoop,
        pid_counter: u16,
        mlx90614:   MLX90614<rp_pico::hal::I2C<pac::I2C1, (gpio::Pin<gpio::pin::bank0::Gpio6, gpio::Function<gpio::I2C>>, gpio::Pin<gpio::pin::bank0::Gpio7, gpio::Function<gpio::I2C>>)>>,
        max6675:    MAX6675<rp_pico::hal::Spi<rp_pico::hal::spi::Enabled, pac::SPI0, 8>, gpio::Pin<gpio::pin::bank0::Gpio17, gpio::PushPullOutput>>,
        uart_0:     UartPeripheral<rp_pico::hal::uart::Enabled, rp_pico::hal::pac::UART0, (gpio::Pin<gpio::pin::bank0::Gpio12, gpio::FunctionUart>, gpio::Pin<gpio::pin::bank0::Gpio13, gpio::FunctionUart>)>,
        led0:       gpio::Pin<gpio::pin::bank0::Gpio27, gpio::PushPullOutput>,
        led1:       gpio::Pin<gpio::pin::bank0::Gpio22, gpio::PushPullOutput>,
        led2:       gpio::Pin<gpio::pin::bank0::Gpio20, gpio::PushPullOutput>,
        button0:    gpio::Pin<gpio::pin::bank0::Gpio28, gpio::PullUpInput>,        
        button1:    gpio::Pin<gpio::pin::bank0::Gpio26, gpio::PullUpInput>,        
        button2:    gpio::Pin<gpio::pin::bank0::Gpio21, gpio::PullUpInput>,   
        relay:      gpio::Pin<gpio::pin::bank0::Gpio19, gpio::PushPullOutput>,
        relay_final: u16,
        relay_counter: u16,
    }

    #[monotonic(binds = TIMER_IRQ_0, default = true)]
    type Monotonic = Rp2040Monotonic;

    // =============================================================================
    // Init task
    #[init(local = [])]
    fn init(cx: init::Context) -> (Shared, Local, init::Monotonics) {
        // =============================================================================
        // Configure the microcontroller
        let mut pac = cx.device;
        let mut watchdog = Watchdog::new(pac.WATCHDOG);
        let sio = Sio::new(pac.SIO);

        // External high-speed crystal on the pico board is 12Mhz
        let external_xtal_freq_hz = 12_000_000u32;
        let clocks = init_clocks_and_plls(
            external_xtal_freq_hz,
            pac.XOSC,
            pac.CLOCKS,
            pac.PLL_SYS,
            pac.PLL_USB,
            &mut pac.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        // =============================================================================
        // Configure GPIO
        let pins = rp_pico::Pins::new(
            pac.IO_BANK0,
            pac.PADS_BANK0,
            sio.gpio_bank0,
            &mut pac.RESETS,
        );

        // Interal Led
        // let mut led_internal = pins.led.into_push_pull_output();

        // External Leds
        let led0 = pins.gpio27.into_push_pull_output();
        let led1 = pins.gpio22.into_push_pull_output();
        let led2 = pins.gpio20.into_push_pull_output();

        // Buttons
        let button0 = pins.gpio28.into_pull_up_input();
        let button1 = pins.gpio26.into_pull_up_input();
        let button2 = pins.gpio21.into_pull_up_input();

        // Relay pin definition
        let relay = pins.gpio19.into_push_pull_output();

        // =============================================================================
        // Configure UART
        let uart_pins = (
            pins.gpio12.into_mode::<gpio::FunctionUart>(),
            pins.gpio13.into_mode::<gpio::FunctionUart>(),
        );
        
        let uart_0 = rp_pico::hal::uart::UartPeripheral::new(pac.UART0, uart_pins, &mut pac.RESETS)
            .enable(
                rp_pico::hal::uart::common_configs::_115200_8_N_1,
                clocks.peripheral_clock.freq(),
            )
            .unwrap();

        // Debug message
        // uart_0.write_full_blocking(b"UART example\r\n");

        // =============================================================================
        // Configure I2C
        let i2c = I2C::i2c1(
            pac.I2C1,
            pins.gpio6.into_mode(), // I2C1_SDA
            pins.gpio7.into_mode(), // I2C1_SCL
            100.kHz(),
            &mut pac.RESETS,
            clocks.peripheral_clock.freq(),
        );

        let ir_sensor     = MLX90614::new(i2c, 0x5A);

        // =============================================================================
        // Configure SPI
        let _spi_sclk = pins.gpio18.into_mode::<gpio::FunctionSpi>();
        let _spi_miso = pins.gpio16.into_mode::<gpio::FunctionSpi>();
        let spi = rp_pico::hal::Spi::<_, _, 8>::new(pac.SPI0);

        let spi = spi.init(
            &mut pac.RESETS,
            clocks.peripheral_clock.freq(),
            100_000.Hz(),
            &MODE_0,
        );

        let mut spi_cs = pins.gpio17.into_push_pull_output();
        spi_cs.set_high().unwrap();
        let thermo_sensor = MAX6675::new(spi, spi_cs);

        // =============================================================================
        // PIDLoop configuration
        const DELTA_T: f64 = 1.0;
        let mut pid_loop: PidLoop = common::Pid::new(1.0, 1.0, 1.0, DELTA_T);
        pid_loop.set_min_max(0.0, 100.0);

        // =============================================================================
        // RTIC things
        let mono = Rp2040Monotonic::new(pac.TIMER);

        // Start tasks
        mlx90614_readout::spawn().ok();
        max6675_readout::spawn().ok();
        gpio_management::spawn().ok();
        button_management::spawn().ok();
        pid_control::spawn().ok();
        uart_monitoring::spawn().ok();

        (
            Shared {
                // Start the system Off
                system_state: 0,
                // Init to more or less room temperature
                temp_tc: 23.0,
                temp_ir: 23.0,
                temp_amb: 23.0,
                // Minus one can be assumed as Off as the system can't cool
                temp_target: -1.0,
                duty_cycle: 0,
            }, 
            Local {
                pid_loop,
                pid_counter: 0,
                mlx90614: ir_sensor,
                max6675:  thermo_sensor,
                uart_0,
                led0,
                led1,
                led2,
                button0,
                button1,
                button2,
                relay,
                // Set period time to 60 loop.. 60 seconds
                relay_final: 60,
                relay_counter: 0,
            }, 
            init::Monotonics(mono)
        )
    }

    // =============================================================================
    // UART monitoring task
    #[task(shared = [system_state, temp_tc, temp_ir, temp_amb, temp_target, duty_cycle], local = [uart_0])]
    fn uart_monitoring(cx: uart_monitoring::Context) {
        let uart_0      = cx.local.uart_0;

        // Variables to lock (note: no type specified)
        let system_state     = cx.shared.system_state;
        let temp_tc     = cx.shared.temp_tc;
        let temp_ir     = cx.shared.temp_ir;
        let temp_amb    = cx.shared.temp_amb;
        let temp_target = cx.shared.temp_target;
        let duty_cycle  = cx.shared.duty_cycle;

        // Lock variables to ensure safe operation
        (system_state, temp_tc, temp_ir, temp_amb, temp_target, duty_cycle).lock(|system_state, temp_tc, temp_ir, temp_amb, temp_target, duty_cycle| {
            writeln!(uart_0, "TC: {:.1}, IR: {:.1}, AMB: {:.1}, TAR: {:.1}, Duty: {}, State: {}\r", temp_tc, temp_ir, temp_amb, temp_target, duty_cycle, system_state).unwrap();
        });

        // uart_monitoring::spawn_after(10.secs()).ok();
        uart_monitoring::spawn_after(1.secs()).ok();
    }

    // =============================================================================
    // PID control tasks
    #[task(shared = [system_state, temp_tc, temp_ir, temp_target, duty_cycle], local = [pid_loop, pid_counter])]
    fn pid_control(cx: pid_control::Context) {
        // Local
        let pid_loop     = cx.local.pid_loop;
        let pid_counter  = cx.local.pid_counter;

        // Shared
        let system_state = cx.shared.system_state;
        let temp_tc      = cx.shared.temp_tc;
        let temp_ir      = cx.shared.temp_ir;
        let temp_target  = cx.shared.temp_target;
        let duty_cycle   = cx.shared.duty_cycle;
        
        (system_state, temp_tc, temp_ir, temp_target, duty_cycle).lock(|system_state, temp_tc, temp_ir, temp_target, duty_cycle| {
            if *system_state != 0 {
                // Use the highest temp as reference in the PID loop
                let mut highest_temp = *temp_tc;
                if *temp_ir > highest_temp {
                    highest_temp = *temp_ir;
                }

                // Program0 - to 60 then hold for 2 minutes - to 250 then hold for 45 seconds
                if *system_state == 1 {
                    // First stage
                    if *temp_target == -1.0 {
                        *temp_target = 60.0;
                        *pid_counter = 0;
                    }

                    // Second stage
                    if *temp_target == 60.0 && *pid_counter >= 120 {
                        *temp_target = 250.0;
                        *pid_counter = 0;
                    }

                    // Finish
                    if *temp_target == 250.0 && *pid_counter >= 45 {
                        *temp_target = -1.0;
                        *system_state = 0;
                        *pid_counter = 0;
                    }

                    // Counter
                    // If within 10% start counting
                    if highest_temp > (*temp_target * 0.9) {
                        *pid_counter += 1;
                    }
                }

                // Program1 - to 250 then holf for 2 minutes
                if *system_state == 2 {

                }

                *duty_cycle = pid_loop.run(highest_temp as f64, *temp_target as f64) as u8;

            } else {
                *temp_target = -1.0;
                *duty_cycle  = 0;
                *pid_counter = 0;
            }
        });

        pid_control::spawn_after(1.secs()).ok();
    }

    // =============================================================================
    // MLX90614 readout task
    #[task(shared = [temp_ir, temp_amb], local = [mlx90614])]
    fn mlx90614_readout(cx: mlx90614_readout::Context) {
        let sensor_ir = cx.local.mlx90614;

        // Variables to lock
        let temp_ir  = cx.shared.temp_ir;
        let temp_amb = cx.shared.temp_amb;

        (temp_ir, temp_amb).lock(|temp_ir, temp_amb| {
            let temperature_data = sensor_ir.get_temperature();

            // Match for error, return -1 if something went wrong
            let temperature_data = match temperature_data {
                Ok(temperature_data) => temperature_data,
                Err(_error) => (-1.0, -1.0),
            };

            (*temp_ir, *temp_amb) = temperature_data;
        });

        mlx90614_readout::spawn_after(100.millis()).ok();
    }

    // =============================================================================
    // MAX6675 readout task
    #[task(shared = [temp_tc], local = [max6675])]
    fn max6675_readout(mut cx: max6675_readout::Context) {
        let sensor_tc = cx.local.max6675;

        cx.shared.temp_tc.lock(|temp_tc| {
            let temperature_data = sensor_tc.get_temperature();

            // Match for error, return -1 if something went wrong
            let temperature_data = match temperature_data {
                Ok(temperature_data) => temperature_data,
                Err(_error) => (-1.0),
            };

            *temp_tc = temperature_data;
        });
        
        max6675_readout::spawn_after(1.secs()).ok();
    }

    // =============================================================================
    // GPIO management task (leds and relay)
    #[task(shared = [system_state, duty_cycle], local = [led0, led1, led2, relay, relay_final, relay_counter])]
    fn gpio_management(mut cx: gpio_management::Context) {
        // Led0 shows current program running - off if program0 - on if program1
        let led0    = cx.local.led0;
        cx.shared.system_state.lock(|system_state| {
            if *system_state == 0 || *system_state == 1 {
                led0.set_low().unwrap();
            } else if *system_state == 2 {
                led0.set_high().unwrap();
            }
        });

        // =============================================================================
        // Status of system
        // Led1 is used to show if the system is running or not
        let led1 = cx.local.led1;
        cx.shared.system_state.lock(|system_state| {
            if *system_state != 0 {
                led1.toggle().unwrap();
            } else {
                led1.set_low().unwrap();
            }
        });
        
        // =============================================================================
        // Relay duty cycle control
        // Led2 is used to reflect the state of the relay
        let led2 = cx.local.led2;
        let relay = cx.local.relay;
        let relay_final = cx.local.relay_final;
        let relay_counter = cx.local.relay_counter;
        cx.shared.duty_cycle.lock(|duty_cycle| {
            if relay_counter == relay_final {
                *relay_counter = 0;
            }

            let match_value: f32 = (*relay_final as f32 / 100.0) * (*duty_cycle) as f32;
            if *relay_counter < match_value as u16 {
                relay.set_high().unwrap();
                led2.set_high().unwrap();
            } else {
                relay.set_low().unwrap();
                led2.set_low().unwrap();
            }

            *relay_counter += 1;
        });

        gpio_management::spawn_after(1.secs()).ok();
    }

    // =============================================================================
    // Button management
    #[task(shared = [system_state, duty_cycle], local = [button0, button1, button2])]
    fn button_management(mut cx: button_management::Context) {
        // =============================================================================
        // Program selecting
        let button0 = cx.local.button0;
        let button1 = cx.local.button1;
        let button2 = cx.local.button2;
        
        // Button2 is STOP - Button1 is program0 - Button0 is program1
        cx.shared.system_state.lock(|system_state| {
            // If Button2 is not pressed
            if !button2.is_low().unwrap() {
                // Don't allow switching if program is active
                if *system_state == 0 {
                    if button1.is_low().unwrap() {
                        *system_state = 1;
                    } else if button0.is_low().unwrap() {
                        *system_state = 2;
                    }
                }
            } else {
                *system_state = 0;
            }
            
        });

        button_management::spawn_after(100.millis()).ok();
    }
}
